# Software to compute the reliability of unitizing continuous data — the u-Alpha family
## Dr Klaus Krippendorff - Annenberg School for Communication

### For instructions, please refer to user manual `uAlphaSoft/uAlphaSoft/usersmanual/uAphaSoftware.pdf` 
